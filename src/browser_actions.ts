// @ts-ignore
import {browser} from "webextension-polyfill-ts";

export const MENU_UPLOADS = "menuUploads";

export let existingMenus = [];
export let existingUploads = [];


/**
 *  Create a new context menu
 * @returns {number | string}
 * @param properties
 * @param setIcon
 */
export const createMenu = (properties, setIcon = false) => {
    if (properties.id === undefined) {
        console.error("[Lufi Extension] - You must provide an id for the context menu");
    } else {
        if (!_menuExists(properties.id)) {
            properties["contexts"] = ["browser_action"];

            existingMenus.push(properties.id);
            return browser.menus.create(properties);
        }
    }
};

export const _menuExists = (id) => {
    return existingMenus.includes(id);
};

export const removeMenu = async (id) => {
    if (_menuExists(id)) {
        return await browser.menus.remove(id);
    }
};

/**
 * Update menu and badge to remove the upload
 * @param id
 * @return {Promise<void>}
 * @private
 */
export const removeUpload = async (id) => {
    existingUploads.splice(existingUploads.indexOf(id), 1);
    await removeMenu(id);
    await updateBadge();
};

export const updateMenu = async (id, properties) => {
    if (_menuExists(id)) {
        await browser.menus.update(id, properties);
    }
};

export const updateBadge = async () => {
    await browser.browserAction.setBadgeText({
        text: existingUploads.length > 0 ? existingUploads.length.toString() : null,
    });
};
