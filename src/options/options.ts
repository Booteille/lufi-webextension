import * as Storage from "../storage";
// @ts-ignore
import {Lufi} from "lufi-js-api";
// @ts-ignore
import * as moment from "moment";

const formDOM = document.getElementById("options-form");

const deleteFile = async (id, delete_on_server = false) => {
    const file = await Storage.getFile(id);
    if (delete_on_server) {
        await Lufi.remove(file);
    }
    await Storage.removeFile(file);
    document.getElementById(id).remove(); // Remove the row
};

const updateDOM = async () => {
    // await updateOptionsDOM();
    await updateTableDOM();
};

const updateOptions = async (e) => {
    const target = e.target;

    await Storage.saveOption(target.id, target.type === "checkbox" ? target.checked : target.value);
};

const updateOptionsDOM = async () => {
    const options = await Storage.loadOptions();

    formDOM.childNodes.forEach((fieldSetElement: HTMLFieldSetElement) => {
        fieldSetElement.childNodes.forEach((node: HTMLElement) => {
            if (options[node.id] !== undefined) {
                if (node instanceof HTMLInputElement) {
                    if (node.type === "checkbox") {
                        node.checked = options[node.id];
                    } else {
                        node.value = options[node.id];
                    }
                } else if (node instanceof HTMLSelectElement) {
                    node.value = options[node.id];
                }
            }

        });
    });
};

const updateTableDOM = async () => {
    const files = await Storage.loadFiles();

    const filesTbody = document.getElementById("files-tbody") as HTMLTableSectionElement;

    // Empty rows
    while (filesTbody.hasChildNodes()) {
        filesTbody.removeChild(filesTbody.lastChild);
    }

    let i = 0;
    files.forEach((file) => {
        let row = filesTbody.insertRow();
        row.id = file.local_id;
        row.insertCell(0).innerHTML = file.name;
        row.insertCell(
            1).innerHTML = `<a class="download-link" href="${file.downloadLink}" rel="noreferrer" rel="noopener">&#11123;</a>`;
        let counterRow = row.insertCell(2);
        counterRow.innerHTML = "loading";
        counterRow.className = "counter";
        row.insertCell(3).innerHTML = file.del_at_first_view ? "&#128504;" : "&#x2717;";
        row.insertCell(4).innerHTML = moment.unix(file.created_at).locale(window.navigator.language).format("LLLL");
        row.insertCell(5).innerHTML = file.delay === 0 ? "&#x2717" : moment.unix(file.delay * 86400 + file.created_at)
                                                                           .locale(
                                                                               window.navigator.language)
                                                                           .format("LLLL");
        let deleteRow = row.insertCell(6);
        deleteRow.innerHTML = `<a class="delete-link" data-local_id="${file.local_id}" href="#${file.deleteLink}" rel="noreferrer" rel="noopener">&#10060;</a>`;
        row.insertCell(
            7).innerHTML = `<a class="mail-link" href="${file.mailLink}" rel="noreferrer" rel="noopener">&#128231;</a>`;

        Lufi.infos(file).then((data) => {
            if (data.success) {
                if (data.deleted) {
                    deleteFile(file.local_id);
                } else {
                    document.getElementById(file.local_id)
                            .getElementsByClassName("counter")
                            .item(0).innerHTML = data.counter;
                }
            } else {
                if (data.missing) {
                    deleteFile(file.local_id);
                }
            }
        });

        deleteRow.addEventListener("click", async (event) => {
            deleteFile((event.target as HTMLAnchorElement).dataset.local_id, true).then();
        });
    });
};

const updateI18nDOM = () => {
    document.querySelectorAll("[data-i18n-text]").forEach((el: HTMLElement) => {
        el.innerText = browser.i18n.getMessage(el.getAttribute("data-i18n-text"));
    });
};

const init = async () => {
    updateI18nDOM();
    await updateOptionsDOM();
    await updateTableDOM();

    browser.storage.onChanged.addListener(updateDOM);
    document.getElementById("options-form").addEventListener("change", updateOptions);
};

try {
    document.addEventListener("DOMContentLoaded", init);
} catch (e) {
    console.error(e);
}
