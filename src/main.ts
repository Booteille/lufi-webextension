// @ts-ignore
import {browser} from "webextension-polyfill-ts";
// @ts-ignore
import {Lufi, Instance} from "lufi-js-api";
import {
    createMenu,
    existingUploads,
    MENU_UPLOADS,
    removeUpload,
    updateBadge,
    updateMenu,
} from "./browser_actions";
import * as Storage from "./storage";
import * as util from "./util";

const onCancel = async (local_id, ws_url, file) => {
    await util.notify(browser.i18n.getMessage("notificationUploadCancelled", [file.name]));
    await removeUpload(local_id);
};

/**
 * Function to be thrown on upload failure
 * @param instance_url
 * @param local_id
 * @param msg
 * @param from_server
 * @return {Promise<void>}
 */
const onFailure = async (instance_url, local_id, msg, from_server = false) => {
    if (msg === undefined) {
        msg = "";
    }

    await util.notify(browser.i18n.getMessage("notificationUploadFailure", [instance_url, msg]));

    if (local_id !== null) {
        await removeUpload(local_id);
    }
};

/**
 * Function to be thrown on upload progress
 * @param instance_url
 * @param local_id
 * @param file
 * @param slices
 * @param current_slice
 * @return {Promise<void>}
 */
const onProgress = async (instance_url, local_id, file, slices, current_slice) => {
    await updateMenu(local_id, {
        title: browser.i18n.getMessage("menuUploadOnProgress", [file.name, current_slice, slices.length]),
    });
};

/**
 * Function to be thrown on new upload
 * @param local_id
 * @param file
 * @return {Promise<void>}
 */
const onInit = async (local_id, file) => {
    existingUploads.push(local_id);

    createMenu({
        id: local_id,
        parentId: MENU_UPLOADS,
        icons: {
            32: "assets/img/ban-solid.svg",
        },
        title: file.name,
        onclick: async () => {
            Lufi.cancelUpload(local_id);
        }
    });
    await updateBadge();
};

/**
 * Function to be thrown if the upload success
 * @param file
 * @param copy_to_clipboard
 * @return {Promise<void>}
 */
const onSuccess = async (file, copy_to_clipboard) => {
    await Storage.storeFile(file);
    await removeUpload(file.local_id);

    if (copy_to_clipboard) {
        await navigator.clipboard.writeText(file.downloadLink);
        console.info("[Lufi Extension] - Download link copied to clipboard");
    }

    await util.notify(browser.i18n.getMessage("notificationUploadSuccess", [file.delay, file.downloadLink]));
};

/**
 * Try to upload a file to a Lufi instance
 * @param e
 * @param copy_to_clipboard
 * @returns {Promise<void>}
 */
const uploadFile = async (e, copy_to_clipboard) => {
    try {
        const options = await Storage.loadOptions();

        let instance = options["random_instance"] ? (await Instance.randomInstance()).endpoint : options["instance_url"];

        Lufi.upload(e.target.files[0], instance, options["delay"], options["del_at_first_view"], onInit,
            onProgress,
            async (file) => onSuccess(file, copy_to_clipboard),
            async (local_id, ws_url, file) => onCancel(local_id, ws_url, file),
            async (file_id, msg) => onFailure(instance, file_id, msg),
        );

        await util.notify(browser.i18n.getMessage("notificationUploadOnProgress", instance));
    } catch (e) {
        console.error(e);

        await util.notify(browser.i18n.getMessage("notificationUploadFailure"));
    }

};

const askFile = (copy_to_clipboard = true) => {
    console.info("[Lufi Extension] - Selecting a file to upload");
    let fileSelector = document.createElement("input");
    fileSelector.setAttribute("type", "file");
    // TODO: Deal with multiple files

    fileSelector.click();

    fileSelector.addEventListener("change", (target) => uploadFile(target, copy_to_clipboard));
};

createMenu({
    id: MENU_UPLOADS,
    icons: {
        32: "assets/img/lufi.svg",
    },
    title: browser.i18n.getMessage(MENU_UPLOADS),
});
createMenu({
    id: "menuCancelAllUploads",
    title: browser.i18n.getMessage("menuCancelAllUploads"),
    parentId: MENU_UPLOADS,
    onclick: async () => {
        Lufi.cancelAllUploads();
        await util.notify(browser.i18n.getMessage("notificationAllUploadsCancelled"));
    },
});
createMenu({
    id: "separator-0",
    parentId: MENU_UPLOADS,
    type: "separator",
});

browser.browserAction.onClicked.addListener(() => {
    askFile();
});

browser.commands.onCommand.addListener((command) => {
    if (command === "upload") {
        askFile();
    }
});
