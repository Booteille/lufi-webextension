// @ts-ignore
import {browser} from "webextension-polyfill-ts";

export const STORAGE_KEY_FILES = "files";
export const STORAGE_KEY_OPTIONS = "options";

export const getFile = async (local_id) => {
    const files = await loadFiles();

    return files.filter((o) => o.local_id === local_id)[0];
};

export const load = async () => {
    try {
        return (await browser.storage.local.get());
    } catch (e) {
        console.error(e);
    }
};

export const loadFiles = async () => {
    try {
        return (await load())[STORAGE_KEY_FILES] || [];
    } catch (e) {
        console.error(e);
    }
};

export const loadOptions = async () => {
    try {
        return (await load())[STORAGE_KEY_OPTIONS] || {
            delay: 1,
            random_instance: true,
            del_at_first_view: false,
        };
    } catch (e) {
        console.error(e);
    }
};

export const removeFile = async (file) => {
    let files = await loadFiles();

    await browser.storage.local.set({[STORAGE_KEY_FILES]: files.filter(o => o.local_id !== file.local_id)});
    console.info(`[Lufi Extension][Storage] - The file "${file.name}" (local id: ${file.local_id} has been removed from local storage`);
};

export const saveOption = async (key, value) => {
    try {
        let options = await loadOptions();

        options[key] = value;

        await browser.storage.local.set({[STORAGE_KEY_OPTIONS]: options});

        console.info("[Lufi Extension][Storage] - Option saved");
    } catch (e) {
        console.error(e.message);
    }
};

export const storeFile = async (file) => {
    try {
        let files = await loadFiles();

        files.push(file);

        await browser.storage.local.set({[STORAGE_KEY_FILES]: files});

        console.info("[Lufi Extension][Storage] - File added to the local storage");
    } catch (e) {
        console.error(e.message);
    }
};

export const clear = async () => {
    try {
        await browser.storage.local.clear();

        console.info("[Lufi Extension][Storage] - Local storage cleared");
    } catch (e) {
        console.error(e);
    }
};

export const remove = async (key) => {
    try {
        await browser.storage.local.remove(key);

        console.info(`[Lufi Extension][Storage] - All elements with key "${key}" removed from the local storage`);
    } catch (e) {
        console.error(e);
    }
};
