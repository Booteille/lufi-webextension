// @ts-ignore
import {browser} from "webextension-polyfill-ts";

export const notify = async (msg) => {
    await browser.notifications.create({
                                           type: "basic",
                                           title: browser.i18n.getMessage("extName"),
                                           message: msg,
                                           iconUrl: browser.runtime.getURL("assets/img/lufi.svg"),
                                       });
};
