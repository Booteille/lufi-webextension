# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.3.0] - 2020-04-13
### Added
* Add a contextual menu opening by right clicking on the icon of the extension (The `menus` permission is now needed)
* Add ability to cancel uploads
* It's now possible to upload multiple files at the same time.
    * Though, it's still not possible to select multiple files at once.
* Add a badge to the icon of the extension showing how many uploads are on the flow.

## [0.2.1] - 2020-04-08
### Added
* Now show notifications from the server in case of upload failure.

### Changed
* Removed hard coded file size limit

## [0.2.0] - 2020-04-08
### Added
* Add option to select delay
* Add option to delete the file after first download
* Add notifications at various steps of the download

### Changed
* Improve notifications
* Limit size of file upload to 1GB
* Improve a bit the settings page

## [0.1.0] - 2020-04-07
First release.
