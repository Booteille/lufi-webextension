# Lufi WebExtension

## About
This extension allows you to upload easily your files to a [Lufi](https://framagit.org/fiat-tux/hat-softwares/lufi/) instance.
This extension is experimental. Use it at your own risks.

## Requirements
**Be sure you allowed popups opening in Firefox settings!**

This is to allow Firefox to open the file selector.

## Build
To build the extension, run the following commands:

```js
npm install
```
then
```js
npm run build
```

## Licences
This extension uses icons from the [Font Awesome](https://fontawesome.com/license) project.
